public class MethodsTest{
	
	public static void main(String args[]){
		SecondClass x = new SecondClass();
		
		//System.out.println(x);
		//double z = sumSquareRoot(10, 26);
		//System.out.println(z);
		//String s1 = "java";
		//String s2 = "programming";
		//System.out.println(s1.length() +" and " + s2.length());
		//SecondClass.addOne(1);
		System.out.println(x.addTwo(50));
	}
	
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
		
		
	}
	
	public static void methodOneInputNoReturn(double value){
		value = value - 5;
		System.out.println("In the method one input no return " + value);
		int x = 20;
		System.out.println(x);
		
		
	}
	public static void methodTwoInputNoReturn(int value, double input){
		
		System.out.println("The inputs are " + value + " and " + input);
		
		
		
		
	}
	public static int methodNoInputOneReturn(){
		
		int y = 5;
		return 5;
		
		
		
		
	}
	public static double sumSquareRoot(int a, int b){
		int x = a + b;
		return Math.sqrt(x);
		
		
	}
}